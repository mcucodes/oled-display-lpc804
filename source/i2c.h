/*
 * i2c.h
 *
 *  Created on: Aug 12, 2018
 *      Author: Ashok
 */

#ifndef I2C_H_
#define I2C_H_

#include "board.h"
#include "pin_mux.h"
#include "fsl_i2c.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define EXAMPLE_I2C_MASTER_BASE (I2C0_BASE)
#define I2C_MASTER_CLOCK_FREQUENCY (12000000)
#define EXAMPLE_I2C_MASTER ((I2C_Type *)EXAMPLE_I2C_MASTER_BASE)

#define I2C_MASTER_SLAVE_ADDR_7BIT (0x7EU)
#define I2C_BAUDRATE (100000) /* 100K */
#define I2C_DATA_LENGTH (33)  /* MAX is 256 */


void I2C_Init();
void I2C_Transfer(uint8_t deviceAddress,uint8_t *txbuffer,uint8_t size);


#endif /* I2C_H_ */
